/*******************************************************************************
 * Copyright (c) 2006 Synchrotron SOLEIL
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.All rights reserved. This program and the accompanying materials
 * 
 * Contributors: vincent.hardion@synchrotron-soleil.fr - initial implementation
 *******************************************************************************/
package fr.soleil.bean.motor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.Collator;
import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.definition.widget.ITextField;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.tango.data.service.AttributePropertyType;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.helper.TangoCommandHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.lib.project.file.BatchExecutor;

/**
 * A bean used to control motors
 * 
 * @author HARDION, AMESYS (cometisation), Saintin (cometisation V2)
 */
public class UserMotorBean extends AbstractTangoBox {

    private static final long serialVersionUID = 5295637769918225737L;

    private static final CometeColor BG_COLOR = new CometeColor(204, 204, 204);;
    private static final CometeFont VIEWER_FONT = new CometeFont(Font.DIALOG, Font.BOLD, 20);
    private static final CometeFont EDITOR_FONT = new CometeFont(Font.DIALOG, Font.PLAIN, 12);
    private static final CometeFont UNIT_FONT = new CometeFont(Font.DIALOG, Font.PLAIN, 12);
    private static final Font BORDER_FONT = new Font(Font.DIALOG, Font.PLAIN, 12);

    private static final String DEFINE_POSITION_TEXT = "\u2190|\u2192";
    private static final String DEFINE_POSITION_TOOLTIP = "Define position";
    private static final String BACKWARD_TEXT = "<<";
    private static final String BACKWARD_TOOLTIP = "Do an Axis Backward";
    private static final String FORWARD_TEXT = ">>";
    private static final String FORWARD_TOOLTIP = "Do an Axis Forward";
    private static final String DECREMENT_AXIS_TEXT = "-";
    private static final String DECREMENT_AXIS_TOOLTIP = "Decrement the axisCurrentPosition with the step value";
    private static final String INCREMENT_AXIS_TEXT = "+";
    private static final String INCREMENT_AXIS_TOOLTIP = "Increment the axisCurrentPosition with the step value";
    private static final String ATK_TEXT = "ATK";
    private static final String STOP_TEXT = "Stop";
    private static final String INCREMENT_VALUE_TEXT = "Increment";
    private static final String INCREMENT_VALUE_TOOLTIP = "Increment Value";
    private static final String GO_TEXT = "GO";
    private static final String GO_TOOLTIP = "Increment the axisCurrentPosition with the step value";
    private static final String RIGHT_TEXT = "Right";
    private static final String LEFT_TEXT = "Left";
    private static final String UP_TEXT = "Up";
    private static final String DOWN_TEXT = "Down";
    private static final String UNIT_TEXT = "Unit";

    // Parameter
    /** Holds value of property axisHorizontal. */
    private boolean axisHorizontal;
    private double targetValue;
    /** option d'affichage **/
    private final boolean simpleMode;
    private String simpleName = null;

    /**
     * The batch executed when clicking on control panel
     */
    private String executedBatchFile;
    private final BatchExecutor batchExecutor;

    // Swing widget fixed elements
    private JPanel centerPanel;
    private JPanel simpleCenterPanel;
    private JPanel topCenterPanel;
    private JLabel incrementLabel;
    private JButton goButton;
    private JButton backwardButton;
    private JButton forwardButton;
    private JButton decButton;
    private JButton incButton;
    private JButton controlPanelButton;
    private JLabel consigneLabel;

    // Comete Widget
    private TextField incrementValue;
    private StringButton stopCommandViewer;
    private Label positionScalarViewer;
    private WheelSwitch positionScalarWheelEditor;
    private Label unitLabel;
    private StringButton definePositionViewer;

    // Comete Box
    private final NumberScalarBox numberBox;
    private boolean confirmation;
    private String attributeName;
    private TangoKey attributeKey;

    // AbstractDataSource
    private AbstractDataSource<?> forwardCmd;
    private AbstractDataSource<?> backwardCmd;

    private final Object commandLock;

    private final Border border;

    private boolean safeMode;

    /**
     * Creates new form UserMotorBean
     * 
     **/
    public UserMotorBean() {
        this(false);
    }

    public UserMotorBean(String simpleName) {
        this();
        setSimpleName(simpleName);
    }

    /**
     * Creates new form UserMotorBean
     * 
     **/
    public UserMotorBean(final boolean enableSimpleMode) {
        super();
        batchExecutor = new BatchExecutor();
        border = getBorder();
        commandLock = new Object();
        targetValue = 0;
        executedBatchFile = null;
        centerPanel = null;
        simpleCenterPanel = null;
        topCenterPanel = null;
        incrementLabel = null;
        goButton = null;
        backwardButton = null;
        forwardButton = null;
        decButton = null;
        incButton = null;
        controlPanelButton = null;
        consigneLabel = null;
        incrementValue = null;
        stopCommandViewer = null;
        positionScalarViewer = null;
        positionScalarWheelEditor = null;
        unitLabel = null;
        definePositionViewer = null;
        numberBox = new NumberScalarBox();
        confirmation = false;
        attributeName = null;
        attributeKey = null;
        forwardCmd = null;
        backwardCmd = null;
        simpleMode = enableSimpleMode;
        axisHorizontal = true;
        initGUI();
    }

    public String getAttributeName() {
        if (attributeName == null) {
            attributeName = deviceBundle.getString("UserMotorBean.attribut.position");
        }
        return attributeName;
    }

    public void setAttributeName(final String attributeName) {
        this.attributeName = attributeName;
    }

    public void setConfirmation(final boolean confirmation) {
        this.confirmation = confirmation;
        stringBox.setConfirmation(incrementValue, confirmation);
        stringBox.setConfirmation(definePositionViewer, confirmation);
        numberBox.setConfirmation(positionScalarWheelEditor, confirmation);
    }

    public boolean isConfirmation() {
        return confirmation;
    }

    public String getExecutedBatchFile() {
        return executedBatchFile;
    }

    public void setExecutedBatchFile(String executedBatchFile) {
        this.executedBatchFile = executedBatchFile;
        String batch = executedBatchFile;
        if (batch != null) {
            if (batch.endsWith("{DEVICE}")) {
                executedBatchFile = batch.substring(0, batch.lastIndexOf("{DEVICE}"));
            }
            if (!batch.trim().isEmpty()) {
                batchExecutor.setBatch(executedBatchFile);
            }
        }
        updateControlPanelButtonVisibility();
    }

    private void updateControlPanelButtonVisibility() {
        getAtkButton().setVisible((executedBatchFile != null) && (!executedBatchFile.trim().isEmpty()));
    }

    @Override
    protected void clearGUI() {
        cleanStateModel();
        cleanWidget(getIncrementValue());
        cleanWidget(getPositionScalarWheelEditor());
        cleanWidget(getPositionScalarViewer());
        cleanWidget(getPositionScalarWheelEditor());
        cleanWidget(getUnitLabel());
        cleanWidget(getStopCommandViewer());
        synchronized (commandLock) {
            cleanWidget(getDefinePositionViewer());
            forwardCmd = null;
            backwardCmd = null;
        }
    }

    @Override
    protected void refreshGUI() {
        if ((getModel() != null) && (!getModel().trim().isEmpty())) {
            String propertyValue = null;
            TangoKey tangoKey = null;
            // read increment device properties
            propertyValue = deviceBundle.getString("UserMotorBean.property.increment");
            tangoKey = generatePropertyKey(propertyValue);
            setWidgetModel(getIncrementValue(), stringBox, tangoKey);

            // Batch parameter
            batchExecutor.setBatchParameters(Arrays.asList(getModel()));

            // READ POSITION ATTRIBUTE
            propertyValue = getAttributeName();
            attributeKey = generateAttributeKey(propertyValue);
            setWidgetModel(getPositionScalarViewer(), stringBox, this.attributeKey);
            stringBox.setUnitEnabled(getPositionScalarViewer(), false);

            // WRITE POSITION ATTRIBUTE
            tangoKey = generateWriteAttributeKey(propertyValue);
            setWidgetModel(getPositionScalarWheelEditor(), this.numberBox, tangoKey);

            // READ POSITION UNIT
            tangoKey = generateAttributePropertyKey(propertyValue, AttributePropertyType.UNIT);

            setWidgetModel(getUnitLabel(), stringBox, tangoKey);
            stringBox.setColorEnabled(getUnitLabel(), false);

            // - AxisStop Command
            propertyValue = deviceBundle.getString("UserMotorBean.command.stop");
            tangoKey = generateCommandKey(propertyValue);
            setWidgetModel(getStopCommandViewer(), this.stringBox, tangoKey);
            getStopCommandViewer().setText(STOP_TEXT);

            // XXX GIRARDOT: why a Thread ? code temporary deactivated
            // final Thread commandThread = new
            // Thread(UserMotorBean.class.getSimpleName() + " connecting
            // commands of "
            // + this.getModel()) {
            // @Override
            // public void run() {
            synchronized (commandLock) {
                String property = null;
                TangoKey key = null;
                if (!simpleMode) {
                    property = deviceBundle.getString("UserMotorBean.command.defineposition");
                    // - Define Position Command
                    key = generateCommandKey(property);
                    final IDataSourceProducer producer = getProducer();
                    if (producer != null) {
                        final boolean isCreatable = producer.isSourceCreatable(key);
                        // System.out.println("isCreatable=" + isCreatable);
                        if (isCreatable && !safeMode) {
                            setWidgetModel(getDefinePositionViewer(), stringBox, key);
                            getDefinePositionViewer().setText(DEFINE_POSITION_TEXT);
                        }
                        getDefinePositionViewer().setEnabled(isCreatable && !safeMode);
                    }
                }

                // SetModel for StateViewer
                key = UserMotorBean.this.generateAttributeKey("State");
                UserMotorBean.this.setStateModel();

                // For some motor Backward and Forward Command does not
                // exist XPSAxis
                final IDataSourceProducer producer = getProducer();
                if (producer != null) {
                    property = deviceBundle.getString("UserMotorBean.command.backward");
                    if (TangoCommandHelper.doesCommandExist(getModel(), property)) {
                        key = generateCommandKey(property);
                        try {
                            // Backward Command
                            backwardCmd = producer.createDataSource(key);
                        } catch (final Exception e) {
                            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                    .warn(UserMotorBean.this.getClass().getSimpleName()
                                            + " failed to connect to backward command", e);
                        }
                        getBackwardButton().setVisible(true);
                    } else {
                        getBackwardButton().setVisible(false);
                    }

                    property = deviceBundle.getString("UserMotorBean.command.forward");
                    if (TangoCommandHelper.doesCommandExist(getModel(), property)) {
                        key = generateCommandKey(property);
                        try {
                            // Forward Command
                            forwardCmd = producer.createDataSource(key);
                        } catch (final Exception e) {
                            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                    .warn(UserMotorBean.this.getClass().getSimpleName()
                                            + " failed to connect to forward command", e);
                        }
                        getForwardButton().setVisible(true);
                    } else {
                        getForwardButton().setVisible(false);
                    }
                }
            }
            // }
            // };
            this.getDefinePositionViewer().setText(DEFINE_POSITION_TEXT);
            // commandThread.start();
        }

        // - Getting Device Alias
        String alias = getModel();
        try {
            Database db = ApiUtil.get_db_obj();
            db.setAccessControl(TangoConst.ACCESS_WRITE);
            alias = db.get_alias_from_device(getModel());

        } catch (final DevFailed df) {
            // no alias for this device: not really important
            if (simpleName != null) {
                alias = simpleName;
            } else {
                alias = getModel();
            }
        }

        setBorder(BorderFactory.createTitledBorder(border, alias, TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, BORDER_FONT, Color.RED));
        revalidate();
        repaint();
    }

    private JPanel getTopCenterPanel() {
        if (topCenterPanel == null) {
            topCenterPanel = new JPanel(new BorderLayout());
            topCenterPanel.add(getBackwardButton(), BorderLayout.WEST);
            topCenterPanel.add(getStateLabel(), BorderLayout.CENTER);
            topCenterPanel.add(getForwardButton(), BorderLayout.EAST);

        }
        return topCenterPanel;
    }

    /**
     * This method initializes simpleCenterPanel
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getSimpleCenterPanel() {
        if (simpleCenterPanel == null) {
            simpleCenterPanel = new JPanel();
            simpleCenterPanel.setLayout(new BoxLayout(simpleCenterPanel, BoxLayout.Y_AXIS));

            final JPanel positionScalarPanel = new JPanel(new GridLayout(2, 1));
            positionScalarPanel.add(getPositionScalarViewer());
            positionScalarPanel.add(getPositionScalarWheelEditor());

            final JPanel incrementAndStopPanel = new JPanel(new GridLayout(1, 2));
            incrementAndStopPanel.add(getIncrementValue());
            incrementAndStopPanel.add(getStopCommandViewer());

            final JPanel goPanel = new JPanel(new GridBagLayout());
            // goPanel.setPreferredSize(new Dimension(400, 60));
            JButton goButton = getGoButton();
            // goButton.setMaximumSize(new Dimension(2000, 20));
            // goButton.setMinimumSize(new Dimension(200, 20));
            // goButton.setPreferredSize(new Dimension(200, 20));
            goPanel.add(goButton);

            simpleCenterPanel.add(positionScalarPanel);
            simpleCenterPanel.add(incrementAndStopPanel);
            simpleCenterPanel.add(goPanel);
        }
        return simpleCenterPanel;
    }

    /**
     * 
     * @param tagetValue
     */
    public void setTargetValue(final double tagetValue) {
        this.targetValue = tagetValue;
    }

    /**
     * 
     * @return TextField
     */
    private TextField getIncrementValue() {
        if (incrementValue == null) {
            incrementValue = generateTextField();
            incrementValue.setActionPerformedOnTextChanged(true);
            incrementValue.setToolTipText(INCREMENT_VALUE_TOOLTIP);
        }
        return incrementValue;
    }

    /**
     * This method initializes centerPanel
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getCenterPanel() {
        if (centerPanel == null) {
            centerPanel = new JPanel();
            centerPanel.setLayout(new GridBagLayout());

            // FIRST LINE Y = 0
            // DECREMENT BUTTON
            final GridBagConstraints decButtonConstraints = new GridBagConstraints();
            decButtonConstraints.fill = GridBagConstraints.BOTH;
            decButtonConstraints.gridx = 0;
            decButtonConstraints.gridy = 0;
            decButtonConstraints.gridheight = 2;
            decButtonConstraints.insets = new Insets(0, 0, 0, 5);
            centerPanel.add(getDecButton(), decButtonConstraints);

            // TOP CENTER PANEL - 1 0
            final GridBagConstraints topCenterPanelConstraints = new GridBagConstraints();
            topCenterPanelConstraints.fill = GridBagConstraints.BOTH;
            topCenterPanelConstraints.gridx = 1;
            topCenterPanelConstraints.gridy = 0;
            topCenterPanelConstraints.gridwidth = 3;
            topCenterPanelConstraints.insets = new Insets(0, 2, 0, 5);
            centerPanel.add(getTopCenterPanel(), topCenterPanelConstraints);

            // // BACKWARD BUTTON - 1 0
            // final GridBagConstraints backwardButtonConstraints = new
            // GridBagConstraints();
            // backwardButtonConstraints.fill = GridBagConstraints.BOTH;
            // backwardButtonConstraints.gridx = 1;
            // backwardButtonConstraints.gridy = 0;
            // centerPanel.add(getBackwardButton(), backwardButtonConstraints);
            //
            // // STATE LABEL - 2 0
            // final GridBagConstraints stateLabelConstraints = new
            // GridBagConstraints();
            // stateLabelConstraints.fill = GridBagConstraints.BOTH;
            // stateLabelConstraints.gridx = 2;
            // stateLabelConstraints.gridy = 0;
            // stateLabelConstraints.gridwidth = 2;
            // centerPanel.add(getStateLabel(), stateLabelConstraints);
            //
            // // FORWARD BUTTON - 4 0
            // final GridBagConstraints forwardButtonConstraints = new
            // GridBagConstraints();
            // forwardButtonConstraints.fill = GridBagConstraints.BOTH;
            // forwardButtonConstraints.gridx = 4;
            // forwardButtonConstraints.gridy = 0;
            // centerPanel.add(getForwardButton(), forwardButtonConstraints);

            // INCREMENT BUTTON - 4 0
            final GridBagConstraints incButtonConstraints = new GridBagConstraints();
            incButtonConstraints.fill = GridBagConstraints.BOTH;
            incButtonConstraints.gridx = 4;
            incButtonConstraints.gridy = 0;
            incButtonConstraints.gridheight = 2;
            incButtonConstraints.insets = new Insets(0, 0, 0, 5);
            centerPanel.add(getIncButton(), incButtonConstraints);

            // STOP BUTTON - 5 0
            final GridBagConstraints stopCommandViewerConstraints = new GridBagConstraints();
            stopCommandViewerConstraints.fill = GridBagConstraints.BOTH;
            stopCommandViewerConstraints.gridx = 5;
            stopCommandViewerConstraints.gridy = 0;
            stopCommandViewerConstraints.gridheight = 2;
            stopCommandViewerConstraints.anchor = GridBagConstraints.CENTER;
            stopCommandViewerConstraints.insets = new Insets(2, 2, 2, 2);
            centerPanel.add(getStopCommandViewer(), stopCommandViewerConstraints);

            // SECOND LINE Y = 1
            // POSITION LABEL
            final GridBagConstraints positionScalarViewerConstraints = new GridBagConstraints();
            positionScalarViewerConstraints.fill = GridBagConstraints.BOTH;
            positionScalarViewerConstraints.gridx = 1;
            positionScalarViewerConstraints.gridy = 1;
            positionScalarViewerConstraints.gridwidth = 3;
            positionScalarViewerConstraints.insets = new Insets(2, 2, 2, 5);
            centerPanel.add(getPositionScalarViewer(), positionScalarViewerConstraints);

            // THIRD LINE Y = 2
            // INCREMENT LABEL 0 2
            final GridBagConstraints incrementLabelConstraints = new GridBagConstraints();
            incrementLabelConstraints.gridx = 0;
            incrementLabelConstraints.gridy = 2;
            centerPanel.add(getIncrementLabel(), incrementLabelConstraints);

            // INCREMENT VALUE TEXTFIELD 1 2
            final GridBagConstraints incrementValueConstraints = new GridBagConstraints();
            incrementValueConstraints.gridx = 1;
            incrementValueConstraints.gridy = 2;
            centerPanel.add(getIncrementValue(), incrementValueConstraints);

            // POSITION WHEELEDITOR 2 2
            final GridBagConstraints positionScalarWheelEditorConstraints = new GridBagConstraints();
            positionScalarWheelEditorConstraints.fill = GridBagConstraints.BOTH;
            positionScalarWheelEditorConstraints.gridx = 2;
            positionScalarWheelEditorConstraints.gridy = 2;
            positionScalarWheelEditorConstraints.insets = new Insets(2, 0, 0, 0);
            centerPanel.add(getPositionScalarWheelEditor(), positionScalarWheelEditorConstraints);

            // UNIT LABEL 3 2
            final GridBagConstraints unitLabelConstraints = new GridBagConstraints();
            unitLabelConstraints.fill = GridBagConstraints.BOTH;
            unitLabelConstraints.gridx = 3;
            unitLabelConstraints.gridy = 2;
            unitLabelConstraints.insets = new Insets(2, 0, 0, 5);
            centerPanel.add(getUnitLabel(), unitLabelConstraints);

            // DEFINE POSITION BUTTON 4 2
            final GridBagConstraints definePositionViewerConstraints = new GridBagConstraints();
            definePositionViewerConstraints.gridx = 4;
            definePositionViewerConstraints.gridy = 2;
            definePositionViewerConstraints.insets = new Insets(2, 2, 0, 5);
            centerPanel.add(getDefinePositionViewer(), definePositionViewerConstraints);

            // ATK BUTTON 5 2
            final GridBagConstraints atkButtonConstraints = new GridBagConstraints();
            atkButtonConstraints.gridx = 5;
            atkButtonConstraints.gridy = 2;
            atkButtonConstraints.insets = new Insets(2, 2, 0, 2);
            centerPanel.add(getAtkButton(), atkButtonConstraints);

        }
        return centerPanel;
    }

    /**
     * This method initializes incButton
     * 
     * @return javax.swing.JButton
     */
    private JButton getGoButton() {
        if (goButton == null) {
            goButton = new JButton();
            goButton.setText(GO_TEXT);
            goButton.setToolTipText(GO_TOOLTIP);
            goButton.setFont(new Font(Font.DIALOG, 1, 12));
            goButton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
            goButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent evt) {
                    jButtonIncActionPerformed(evt);
                }
            });
            goButton.setVisible(true);
        }
        return goButton;
    }

    /**
     * This method initializes incButton
     * 
     * @return javax.swing.JButton
     */
    private JButton getIncButton() {
        if (incButton == null) {
            incButton = new JButton();
            incButton.setText(INCREMENT_AXIS_TEXT);
            incButton.setToolTipText(INCREMENT_AXIS_TOOLTIP);
            incButton.setFont(new Font(Font.DIALOG, 1, 18));
            incButton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
            // incButton.setMaximumSize(new Dimension(200, 28));
            // incButton.setMinimumSize(new Dimension(60, 28));
            // incButton.setPreferredSize(new Dimension(60, 28));
            incButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent evt) {
                    jButtonIncActionPerformed(evt);
                }
            });
            incButton.setVisible(true);
        }
        return incButton;
    }

    /**
     * This method initializes backwardButton
     * 
     * @return javax.swing.JButton
     */
    private JButton getBackwardButton() {
        if (backwardButton == null) {
            backwardButton = new JButton();
            backwardButton.setText(BACKWARD_TEXT);
            backwardButton.setToolTipText(BACKWARD_TOOLTIP);
            // backwardButton.setPreferredSize(new Dimension(40, 20));
            // backwardButton.setMinimumSize(new Dimension(40, 20));
            // backwardButton.setMaximumSize(new Dimension(150, 20));
            backwardButton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
            backwardButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(final MouseEvent evt) {
                    leftButtonMousePressed(evt);
                }

                @Override
                public void mouseReleased(final MouseEvent evt) {
                    leftButtonMouseReleased(evt);
                }
            });
            backwardButton.setVisible(false);
        }

        return this.backwardButton;
    }

    /**
     * This method initializes forwardButton
     * 
     * @return javax.swing.JButton
     */
    private JButton getForwardButton() {
        if (forwardButton == null) {
            forwardButton = new JButton();
            forwardButton.setText(FORWARD_TEXT);
            forwardButton.setToolTipText(FORWARD_TOOLTIP);
            // forwardButton.setPreferredSize(new Dimension(40, 20));
            // forwardButton.setMinimumSize(new Dimension(40, 20));
            // forwardButton.setMaximumSize(new Dimension(150, 20));
            forwardButton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
            forwardButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(final MouseEvent evt) {
                    rightButtonMousePressed(evt);
                }

                @Override
                public void mouseReleased(final MouseEvent evt) {
                    rightButtonMouseReleased(evt);
                }
            });
            forwardButton.setVisible(false);
        }
        return forwardButton;
    }

    /**
     * This method initializes decButton
     * 
     * @return javax.swing.JButton
     */
    private JButton getDecButton() {
        if (decButton == null) {
            decButton = new JButton();
            decButton.setText(DECREMENT_AXIS_TEXT);
            decButton.setToolTipText(DECREMENT_AXIS_TOOLTIP);
            decButton.setFont(new Font(Font.DIALOG, 1, 18));
            // decButton.setPreferredSize(new Dimension(60, 28));
            // decButton.setMinimumSize(new Dimension(60, 28));
            // decButton.setMaximumSize(new Dimension(200, 28));
            decButton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
            decButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent evt) {
                    jButtonDecActionPerformed(evt);
                }
            });
        }
        return decButton;
    }

    /**
     * This method initializes atkButton
     * 
     * @return javax.swing.JButton
     */
    private JButton getAtkButton() {
        if (controlPanelButton == null) {
            controlPanelButton = new JButton(ATK_TEXT);
            controlPanelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    controlPanelActionPerformed();
                }
            });
        }
        return controlPanelButton;
    }

    /**
     * This method initializes stopCommandViewer
     * 
     * @return fr.soleil.comete.widget.StringButton
     */
    private StringButton getStopCommandViewer() {
        if (stopCommandViewer == null) {
            stopCommandViewer = generateStringButton();
            stringBox.setSettable(stopCommandViewer, false);
            stopCommandViewer.setText(STOP_TEXT);
            stopCommandViewer.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED),
                    BorderFactory.createLineBorder(Color.RED, 2)));
        }
        return stopCommandViewer;
    }

    /**
     * This method initializes positionScalarViewer
     * 
     * @return fr.soleil.comete.widget.NumberField
     */
    private Label getPositionScalarViewer() {
        if (positionScalarViewer == null) {
            positionScalarViewer = generateLabel();
            // positionScalarViewer.setPreferredSize(6, 55);
            positionScalarViewer.setCometeFont(VIEWER_FONT);
            positionScalarViewer.setHorizontalAlignment(ITextField.CENTER);
            // positionScalarViewer.setMinimumSize(new Dimension(6, 55));
        }
        return positionScalarViewer;
    }

    /**
     * This method initializes positionScalarWheelEditor
     * 
     * @return fr.soleil.comete.widget.WheelSwitch
     */
    private WheelSwitch getPositionScalarWheelEditor() {
        if (positionScalarWheelEditor == null) {
            positionScalarWheelEditor = generateWheelSwitch();
            positionScalarWheelEditor.setPreferredSize(100, 50);
            positionScalarWheelEditor.setCometeFont(EDITOR_FONT);
            positionScalarWheelEditor.setOpaque(false);
            // positionScalarWheelEditor.setMinimumSize(new Dimension(100, 50));
            positionScalarWheelEditor.setCometeBackground(BG_COLOR);
            positionScalarWheelEditor.setInvertionLogic(true);
        }
        return positionScalarWheelEditor;
    }

    /**
     * This method initializes incrementButton
     * 
     * @return javax.swing.JButton
     */
    private JLabel getIncrementLabel() {
        if (this.incrementLabel == null) {
            this.incrementLabel = new JLabel(INCREMENT_VALUE_TEXT);
        }
        return this.incrementLabel;
    }

    /**
     * This method initializes definePositionButton
     * 
     * @return A {@link StringButton}
     */
    public StringButton getDefinePositionViewer() {
        if (definePositionViewer == null) {
            definePositionViewer = generateStringButton();
            // definePositionViewer.setPreferredSize(new Dimension(65, 30));
            definePositionViewer.setText(DEFINE_POSITION_TEXT);
            definePositionViewer.setToolTipText(DEFINE_POSITION_TOOLTIP);
            stringBox.setOutputInPopup(this.definePositionViewer, true);
            stringBox.setUseErrorText(this.definePositionViewer, false);
        }
        return definePositionViewer;
    }

    /** Disable dangerous components **/
    public void disableBean() {
        this.safeMode = true;
        if (incrementValue != null) {
            incrementValue.setEditable(false);
        }
        if (incButton != null) {
            incButton.setEnabled(false);
        }
        if (forwardButton != null) {
            forwardButton.setEnabled(false);
        }
        if (backwardButton != null) {
            backwardButton.setEnabled(false);
        }
        if (decButton != null) {
            decButton.setEnabled(false);
        }

        getDefinePositionViewer().setEnabled(false);

        stringBox.setUserEnabled(getIncrementValue(), false);
        numberBox.setUserEditable(getPositionScalarWheelEditor(), false);
        stringBox.setUnitEnabled(getPositionScalarViewer(), false);
        stringBox.setColorEnabled(getUnitLabel(), false);

    }

    private void axisBackward() {
        synchronized (commandLock) {
            if (backwardCmd != null) {
                try {
                    backwardCmd.setData(null);
                } catch (final Exception e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .error(getClass().getSimpleName() + " failed to execute backward command", e);
                }
            } else {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .warn(getClass().getSimpleName() + " couldn't execute backward command");
            }
        }
    }

    private void axisForward() {
        synchronized (commandLock) {
            if (forwardCmd != null) {
                try {
                    forwardCmd.setData(null);
                } catch (final Exception e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .error(getClass().getSimpleName() + " failed to execute forward command", e);
                }
            } else {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .warn(getClass().getSimpleName() + " couldn't execute forward command");
            }
        }
    }

    public String getVersNumber() {
        String versionText, versNumber;
        int colon_idx, dollar_idx;

        versionText = new String("$Revision: 1.1 $");

        colon_idx = versionText.lastIndexOf(":");
        dollar_idx = versionText.lastIndexOf("$");
        versNumber = versionText.substring(colon_idx + 1, dollar_idx);

        return versNumber;
    }

    /**
     * Initializes the GUI.
     */
    public void initGUI() {
        setLayout(new BorderLayout());
        // setPreferredSize(new Dimension(505, 160));
        if (simpleMode) {
            add(getSimpleCenterPanel(), BorderLayout.CENTER);
        } else {
            add(getCenterPanel(), BorderLayout.CENTER);
        }
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(final KeyEvent evt) {
                userMotorBeanKeyPressed(evt);
            }

            @Override
            public void keyReleased(final KeyEvent evt) {
                userMotorBeanKeyReleased(evt);
            }
        });
    }

    // ----------------------------------------------------------------------------------------------
    /**
     * Getter for property axisHorizontal.
     * 
     * @return Value of property axisHorizontal.
     */
    public boolean isAxisHorizontal() {
        return axisHorizontal;
    }

    /**
     * When control Button panel is actionned
     */
    public void controlPanelActionPerformed() {
        String commandToExecute = File.separator.equals("/") ? "atkpanel " : "atkpanel.bat ";
        batchExecutor.setBatch(commandToExecute);
        batchExecutor.execute();
    }

    private void jButtonDecActionPerformed(final ActionEvent evt) {
        if ((model != null) && (!model.trim().isEmpty())) {
            double position = 0;
            final AbstractDataSource<Number> dataSource = getNumberSource(attributeKey);
            try {
                if (dataSource != null) {
                    final Number data = dataSource.getData();
                    if (data != null) {
                        position = data.doubleValue();
                        position -= getIncrementDoubleValue();
                        dataSource.setData(position);
                    }
                }
            } catch (final Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .error(getClass().getSimpleName() + " failed to decrement position", e);
            }
        }
    }

    private double getIncrementDoubleValue() {
        double incValue = 0;
        final String strValue = getIncrementValue().getText();
        try {
            incValue = Double.parseDouble(strValue);
        } catch (final Exception e) {
            incValue = 0;
        }
        return incValue;

    }

    public void gotoTargetPosition() {
        final ActionEvent evt = null;
        this.jButtonIncActionPerformed(evt);
    }

    public void stopTargetPosition() {
        final ActionEvent evt = null;
        this.jButtonStopCommandViewer(evt);
    }

    @SuppressWarnings("unchecked")
    private AbstractDataSource<Number> getNumberSource(final IKey key) {
        final IDataSourceProducer producer = getProducer();
        AbstractDataSource<Number> dataSource = null;
        AbstractDataSource<?> src = null;
        try {
            src = producer.createDataSource(key);
            if ((src != null) && (src.getDataType() != null)
                    && Number.class.equals(src.getDataType().getConcernedClass())) {
                dataSource = (AbstractDataSource<Number>) src;
            }
        } catch (final Exception e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getSimpleName()
                    + " failed to recover data source for key " + (key == null ? null : key.getInformationKey()), e);
        }

        return dataSource;
    }

    private void jButtonStopCommandViewer(final ActionEvent evt) {
        if ((model != null) && (!model.trim().isEmpty())) {
            getStopCommandViewer().execute();
        }
    }

    private void jButtonIncActionPerformed(final ActionEvent evt) {
        if ((model != null) && (!model.trim().isEmpty())) {
            double position = 0;
            final AbstractDataSource<Number> dataSource = getNumberSource(this.attributeKey);
            final double increment = getIncrementDoubleValue();
            Number data;
            try {
                data = dataSource.getData();
                if (data != null) {
                    position = data.doubleValue();
                }
            } catch (final Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .error(getClass().getSimpleName() + " failed to read position", e);
            }
            if (this.simpleMode) {
                final double distance = Math.abs(targetValue - position);
                if (distance > 0.00001) {
                    if (increment == 0) {
                        position = targetValue;
                    } else if (targetValue != position) {
                        if (distance > increment) {
                            position += increment * Math.signum(targetValue - position);
                        } else {
                            position = targetValue;
                        }
                    }
                }
            } else {
                position += increment;
            }

            try {
                if (dataSource != null) {
                    dataSource.setData(position);
                }
            } catch (final Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .error(getClass().getSimpleName() + " failed to send position", e);
            }
        }
    }

    private void leftButtonMousePressed(final MouseEvent evt) {
        axisBackward();
    }

    private void leftButtonMouseReleased(final MouseEvent evt) {
        getStopCommandViewer().execute();
    }

    private void rightButtonMousePressed(final MouseEvent evt) {
        axisForward();
    }

    private void rightButtonMouseReleased(final MouseEvent evt) {
        getStopCommandViewer().execute();
    }

    /**
     * Setter for property axisHorizontal.
     * 
     * @param axisHorizontal
     *            New value of property axisHorizontal.
     */
    public void setAxisHorizontal(final boolean axisHorizontal) {
        this.axisHorizontal = axisHorizontal;
        if (axisHorizontal) {
            forwardButton.setText(RIGHT_TEXT);
            backwardButton.setText(LEFT_TEXT);
        } else {
            forwardButton.setText(UP_TEXT);
            backwardButton.setText(DOWN_TEXT);
        }
    }

    /**
     * Key pressed event received
     * 
     * @param evt
     *            : Key pressed event
     */
    protected void userMotorBeanKeyPressed(final KeyEvent evt) {
        final int keyCode = evt.getKeyCode();
        if (keyCode == KeyEvent.VK_LEFT) {
            axisBackward();
        } else if (keyCode == KeyEvent.VK_RIGHT) {
            axisForward();
        }
    }

    /**
     * Key released event received
     * 
     * @param evt
     *            : Key released event
     */
    protected void userMotorBeanKeyReleased(final KeyEvent evt) {
        final int keyCode = evt.getKeyCode();
        if ((keyCode == KeyEvent.VK_LEFT) || (keyCode == KeyEvent.VK_RIGHT)) {
            getStopCommandViewer().execute();
        }
    }

    public Label getUnitLabel() {
        if (unitLabel == null) {
            unitLabel = new Label();
            unitLabel.setCometeFont(UNIT_FONT);
            unitLabel.setText(UNIT_TEXT);
        }
        return unitLabel;
    }

    public JLabel getConsigneLabel() {
        if (consigneLabel == null) {
            consigneLabel = new JLabel();
            consigneLabel.setText("0");
        }
        return consigneLabel;
    }

    @Override
    protected void onConnectionError() {
    }

    @Override
    protected void savePreferences(final Preferences preferences) {
    }

    @Override
    protected void loadPreferences(final Preferences preferences) {
    }

    /**
     * This is because of XPS GROUP. This manage the case when have the
     * attribute name with the device name
     */
    @Override
    public void setModel(String model) {
        if (model != null) {
            final String[] split = model.split("/");
            if (split.length > 3) {
                final String entityName = TangoDeviceHelper.getEntityName(model);
                model = TangoDeviceHelper.getDeviceName(model);
                setAttributeName(entityName);
            }
        }
        super.setModel(model);
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public static void main(final String[] args) throws DevFailed {

        final JFrame frame = new JFrame();

        boolean simpleBean = false;
        String title = "Motor Test";
        String model = "";
        if (args.length > 0) {
            model = args[0];
            frame.setTitle(args[0]);
            for (int i = 1; i < args.length; i++) {
                if ("simpleBean".equalsIgnoreCase(args[i])) {
                    simpleBean = true;
                }
            }
        }
        final UserMotorBean bean = new UserMotorBean(simpleBean);
        bean.setModel(model);
        if (!model.isEmpty()) {
            title = title + " " + model;
            bean.setConfirmation(true);
        }
        frame.setTitle(title);
        String tango_root = System.getenv("TANGO_ROOT");
        if ((tango_root == null) || tango_root.trim().isEmpty()) {
            tango_root = "D:\\soleil-root-win32-11.4.2\\tango";
            // tango_root= "Y:";
        }
        // Lien ATK
        String batch = "atkpanel.bat{DEVICE}";
        String batchPath;
        File folder = new File(tango_root + File.separator + "tango");
        if (folder.exists()) {
            batchPath = tango_root + File.separator + "tango" + File.separator + batch;
        } else {
            batchPath = tango_root + File.separator + "bin" + File.separator /*+ "win32" + File.separator*/ + batch;
        }

        bean.setExecutedBatchFile(batchPath);
        bean.start();
        final JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(bean, BorderLayout.CENTER);
        Database db = ApiUtil.get_db_obj();
        Collection<String> models = new TreeSet<>(Collator.getInstance());
        models.add("");
        String[] devices = null;
        try {
            devices = db.get_device_exported_for_class("GalilAxis");
        } catch (Exception e) {
            devices = null;
        }
        if (devices != null) {
            for (String device : devices) {
                models.add(device);
            }
        }
        try {
            devices = db.get_device_exported_for_class("Motor");
        } catch (Exception e) {
            devices = null;
        }
        if (devices != null) {
            for (String device : devices) {
                models.add(device);
            }
        }
        if (bean.getModel() != null) {
            models.add(bean.getModel());
        }
        final JComboBox<String> modelComboBox = new JComboBox<>(models.toArray(new String[models.size()]));
        models.clear();
        modelComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        bean.setModel((String) modelComboBox.getSelectedItem());
                    }
                });
            }
        });
        mainPanel.add(modelComboBox, BorderLayout.NORTH);
        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}